# Structure

## config

  - locale set to default to **pt_BR**
  - broadcasting, caching and sessions set to default to **redis**
  - views set to use **React**

## app

  Finance, Inventory and Marketing are domain examples. Each domain should be
  a directory inside app/ and it **must contain** all business logic required
  for the beforementioned domain.
